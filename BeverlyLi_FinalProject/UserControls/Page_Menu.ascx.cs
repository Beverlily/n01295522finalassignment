﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace BeverlyLi_FinalProject.UserControls
{
    public partial class Page_Menu : System.Web.UI.UserControl
    {
        private string page_select_query = "SELECT page_id, page_title AS Title FROM pages";

        protected void Page_Load(object sender, EventArgs e)
        {
            pages.SelectCommand = page_select_query;
            Page_Manual_Bind(pages, "page_nav");
        }

        //Based off Christine's Code CRUD basic example, teacherpick code
        void Page_Manual_Bind(SqlDataSource src, string menu)
        {
            Menu pageList = (Menu)FindControl(menu);
            pageList.Items.Clear();
            
            DataView pagesView = (DataView)src.Select(DataSourceSelectArguments.Empty);

            //Adding "Manage Pages" to Menu 
            MenuItem managePages = new MenuItem();
            managePages.Text = "Manage Pages";
            managePages.NavigateUrl = "~/ManagePages";
            pageList.Items.Add(managePages);

            //Adding list of pages from the database to Menu
            foreach (DataRowView row in pagesView)
            {
                //Intercept data rendering from database to make menu items 
                MenuItem page_item = new MenuItem();
                page_item.Text = row["Title"].ToString();
                page_item.NavigateUrl = "~/Page.aspx?page_id=" + row["page_id"];
                pageList.Items.Add(page_item);
            }
        }
    }
}
