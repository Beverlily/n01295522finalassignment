﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Page_Menu.ascx.cs" Inherits="BeverlyLi_FinalProject.UserControls.Page_Menu" %>
<%--Referenced your TeacherPick code--%>

<%--Connects to Database--%>
<asp:SqlDataSource
    runat="server" 
    ID="pages"
    ConnectionString="<%$ ConnectionStrings:pages_sql_con %>">
</asp:SqlDataSource>

<%--Menu to display pages--%>
<asp:Menu ID="page_nav" runat="server"></asp:Menu>

