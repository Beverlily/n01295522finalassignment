﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BeverlyLi_FinalProject
{
    public partial class CreatePage : System.Web.UI.Page
    {
        private string insertQuery = "INSERT INTO pages (page_title, page_author, page_content) VALUES";

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void AddPage(object sender, EventArgs e)
        {
            //Referenced: https://docs.microsoft.com/en-us/dotnet/api/system.net.webutility.htmlencode?view=netframework-4.7.2
            //Inputs from web form
            string n_page_title = System.Net.WebUtility.HtmlEncode(new_page_title.Text.ToString());
            string n_page_content = System.Net.WebUtility.HtmlEncode(new_page_content.Text.ToString());
            string n_page_author = System.Net.WebUtility.HtmlEncode(new_page_author.Text.ToString());

            //Decodes input and escapes single quotes...side note: in a real situation, should escape other special characters to avoid
            //SQL injection 
            n_page_title = System.Net.WebUtility.HtmlDecode(n_page_title).Replace("'", "''");
            n_page_content = System.Net.WebUtility.HtmlDecode(n_page_content).Replace("'", "''");
            n_page_author = System.Net.WebUtility.HtmlDecode(n_page_author).Replace("'", "''");

            insertQuery+= "("
                + "'" + n_page_title + "',"
                + "'" + n_page_author + "',"
                + "'" + n_page_content
                + "')";

            insert_page.InsertCommand = insertQuery;
            insert_page.Insert();

            //reloads page so that menu will update with the new inserted page
            //Referenced: https://stackoverflow.com/questions/17779973/refresh-an-asp-net-page-on-button-click
            Response.Redirect(Request.RawUrl);
        }
    }
}
