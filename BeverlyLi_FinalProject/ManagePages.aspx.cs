﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace BeverlyLi_FinalProject
{
    public partial class Pages : System.Web.UI.Page
    {
        private string sqlBaseQuery = "SELECT page_id, page_title AS 'Page Title', page_author AS Author, page_publication_date AS 'Publication Date' FROM pages";

        protected void Page_Load(object sender, EventArgs e)
        {
            pages.SelectCommand = sqlBaseQuery; 
            page_list.DataSource = Page_Manual_Bind(pages);
            page_list.DataBind();
        }

        //Based off your Student.aspx code 
        //Manual bind to add link to page 
        protected DataView Page_Manual_Bind(SqlDataSource src)
        {
            DataTable pageTable;
            DataView pageView;

            //gets DataView of select of datasource and makes that into a table
            pageTable = ((DataView)src.Select(DataSourceSelectArguments.Empty)).Table;
            pageTable.Columns.Add("Action");

            foreach (DataRow row in pageTable.Rows)
            {
                //<a href="Page.aspx?page_id=__">Page Title</a>
                //Adds link leading to page 
                row["Page Title"] = 
                    "<a href=\"Page.aspx?page_id="
                    + row["page_id"] 
                    + "\">" 
                    + row["Page Title"]
                    + "</a>";
                row["Action"] =
                    //Edit link
                    "<a href=\"UpdatePages.aspx?page_id="
                    + row["page_id"]
                    + "\">"
                    + "Edit"
                    + "</a>"
                    + "\t"
                    //Delete link
                    + "<a href=\"DeletePages.aspx?page_id="
                    + row["page_id"]
                    + "\" onclick= \"return confirm('Are you sure you want to delete the page?')\">"
                    + "Delete"
                    + "</a>";
            }
            //Don't need page_id anymore and don't want to display it to user
            pageTable.Columns.Remove("page_id"); 
            pageView = pageTable.DefaultView;

            return pageView;
        }


        //Referenced Students example
        protected void Search_Pages(object sender, EventArgs e)
        {
            string searchQuery = sqlBaseQuery;
            string search = pageSearch.Text;
           
            //if someone is using search
            if (search!="")
            {
                string likeCondition = "LIKE '%" + search + "%'";

                //searches page title and author 
                searchQuery +=
                    " WHERE page_title " + likeCondition
                    + " OR page_author " + likeCondition;
            }

            pages.SelectCommand = searchQuery;
            page_list.DataSource = Page_Manual_Bind(pages);
            page_list.DataBind();
        }
    }
}