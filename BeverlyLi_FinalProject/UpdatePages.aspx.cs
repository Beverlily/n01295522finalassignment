﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace BeverlyLi_FinalProject
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        private string baseSelectQuery = "SELECT page_title AS 'Page Title', page_author AS 'Page Author', page_publication_date AS 'Page Publication Date', page_content AS 'Page Content' FROM pages";
        
        //Gets page_id from URL 
        private string page_id
        {
            get
            {
                return Request.QueryString["page_id"]; 
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        //Got this code from Christine
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            if (page_id == "" || page_id == null) page_title.InnerHtml = "No page found."; 
            else
            {
                GetPageInformation();
            }
        }

        //Referenced Christine's code
        protected void GetPageInformation()
        {
            //Select command for the SqlDataSource
            update_page.SelectCommand = baseSelectQuery + " WHERE page_id=" + page_id;

            //DataView is a view on the database table that you can bind to other data 
            DataView updatePageView = (DataView)update_page.Select(DataSourceSelectArguments.Empty);
            
            //When no row is found 
            if (updatePageView.ToTable().Rows.Count == 0) page_title.InnerHtml = "No page found.";
            else
            {
                //only one row in DataView, so first row would be current page
                DataRowView currentPage = updatePageView[0];

                var pageTitle = currentPage["Page Title"].ToString();
                var pageAuthor = currentPage["Page Author"].ToString();
                var pageContent = currentPage["Page Content"].ToString();

                page_title.InnerHtml = pageTitle;

                //Fills the text inside the input boxes with the page's current information
                update_page_title.Text = pageTitle;
                update_page_author.Text = pageAuthor;
                update_page_content.Text = pageContent;
            }
        }

        protected void UpdatePage(object sender, EventArgs e)
        {
     
            if (page_id == "" || page_id == null) page_title.InnerHtml = "No page found.";
            else { 

            //Referenced your code from the server_control_into example
            //Also referenced: https://docs.microsoft.com/en-us/dotnet/api/system.net.webutility.htmlencode?view=netframework-4.7.2
           
            //Getting inputs from web form
            //Encodes user input from the form so that the form can accept special characters
                string updated_title = System.Net.WebUtility.HtmlEncode(update_page_title.Text.ToString());
                string updated_author = System.Net.WebUtility.HtmlEncode(update_page_author.Text.ToString());
                string updated_content = System.Net.WebUtility.HtmlEncode(update_page_content.Text.ToString());
             
            //Decodes input and escapes single quotes...side note: in a real situation, should escape other special characters to avoid
            //SQL injection 
                updated_title = System.Net.WebUtility.HtmlDecode(updated_title).Replace("'", "''");
                updated_author = System.Net.WebUtility.HtmlDecode(updated_author).Replace("'", "''");
                updated_content = System.Net.WebUtility.HtmlDecode(updated_content).Replace("'", "''");

                var updateQuery = 
                "UPDATE pages "
                + "SET "
                + "page_title = "
                + "'" + updated_title + "' " 
                + ", page_author = "
                + "'" + updated_author + "' "
                + ", page_content = "
                + "'" + updated_content + "' "
                + "WHERE page_id=" + page_id; 

            update_page.UpdateCommand = updateQuery;
            update_page.Update();

            Response.Redirect("Page.aspx?page_id=" + page_id);
            }
        }

        protected void CancelUpdate(object sender, EventArgs e)
        {
            Response.Redirect("Page.aspx?page_id=" + page_id);
        }
    }
}