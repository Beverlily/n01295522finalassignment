﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="UpdatePages.aspx.cs" Inherits="BeverlyLi_FinalProject.WebForm1" %>
<asp:Content ID="Update_Page" ContentPlaceHolderID="MainContent" runat="server">
    <h2>Update Page</h2>
    <h3 id="page_title" runat="server"></h3>

    <%--Datasource to get information about the page we're updating--%>
    <asp:SqlDataSource runat="server" id="update_page"
        ConnectionString="<%$ ConnectionStrings:pages_sql_con %>">
    </asp:SqlDataSource>

    <%--Web form to get information for updating the page--%>
    <br />
    <div id="update_pages_form" runat="server">
        <div>
            <asp:Label ID="update_page_title_label" runat="server" Text="Page Title:" AssociatedControlID="update_page_title" />
            <br />
            <asp:TextBox ID="update_page_title" runat="server" Name="page_title"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server"
                ControlToValidate="update_page_title"
                ErrorMessage="Must enter a page title.">
            </asp:RequiredFieldValidator>
        </div>
        <br />

        <div>
            <asp:Label ID="update_page_author_label" runat="server" Text="Page Author:" AssociatedControlID="update_page_author" />
            <br />
            <asp:TextBox ID="update_page_author" runat="server" Name="page_author"></asp:TextBox>
        </div>
        <br />

        <div>
            <asp:Label ID="update_page_content_label" runat="server" Text="Page Content:" AssociatedControlID="update_page_content" />
            <br />
            <asp:TextBox ID="update_page_content" runat="server" Name="page_content" TextMode="MultiLine"></asp:TextBox>
        </div>

        <br />
        <asp:Button ID="update_page_btn" runat="server" Text="Update Page" OnClick="UpdatePage"/>

        <asp:Button ID="update_cancel_btn" runat="server" Text="Back to Page" OnClick="CancelUpdate"/>
        <a href="ManagePages" runat="server">Back to Manage Pages</a>
    </div>
</asp:Content>

