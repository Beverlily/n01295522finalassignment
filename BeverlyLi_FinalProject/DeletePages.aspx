﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="DeletePages.aspx.cs" Inherits="BeverlyLi_FinalProject.WebForm2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <%--DataSource which gets information on the page selected--%>
    <div id="message" runat="server"></div>
    <asp:SqlDataSource runat="server"
        id="page_selected"
        ConnectionString="<%$ ConnectionStrings:pages_sql_con %>">
    </asp:SqlDataSource>

</asp:Content>
