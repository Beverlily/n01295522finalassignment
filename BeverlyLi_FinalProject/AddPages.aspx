﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AddPages.aspx.cs" Inherits="BeverlyLi_FinalProject.CreatePage" %>
<asp:Content ID="add_pages" ContentPlaceHolderID="MainContent" runat="server">
    <h2>Add Page</h2
    <%--TAKE OUT COLUMNS LATER @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ --%>
    <asp:SqlDataSource runat="server" id="insert_page"
        ConnectionString="<%$ ConnectionStrings:pages_sql_con %>">
    </asp:SqlDataSource>

    <%--Web Form to get information to add new page--%>
    <div id="add_pages_form" runat="server">
        <%--Page Title--%>
        <div>
            <asp:Label ID="new_page_title_label" runat="server" Text="Page Title:" AssociatedControlID="new_page_title" />
            <br />
            <asp:TextBox ID="new_page_title" runat="server" placeholder="Page Title" Width="300px"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server"
                ControlToValidate="new_page_title"
                ErrorMessage="Must enter a page title.">
            </asp:RequiredFieldValidator>
        </div>
        <br />

        <%--Page Author--%>
        <div>
            <asp:Label ID="new_page_author_label" runat="server" Text="Page Author:" AssociatedControlID="new_page_author" />
            <br />
            <asp:TextBox ID="new_page_author" runat="server" placeholder="Page Author" Width="300px"></asp:TextBox>
        </div>
        <br />

        <%--Page Content--%>
        <div>
            <asp:Label ID="new_page_content_label" runat="server" Text="Page Content:" AssociatedControlID="new_page_content" />
            <br />
            <asp:TextBox ID="new_page_content" runat="server" placeholder="Page Content" TextMode="MultiLine" Width="300px" Height="200px"></asp:TextBox>
        </div>
        <br />
        
        <asp:Button ID="add_page_button" runat="server" Text="Add Page" OnClick="AddPage"/>
        <a href="ManagePages">Cancel</a>
    </div>
</asp:Content>