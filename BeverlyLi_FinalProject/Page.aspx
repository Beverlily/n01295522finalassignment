﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Page.aspx.cs" Inherits="BeverlyLi_FinalProject.Page" %>

<asp:Content runat="server" id="page" ContentPlaceHolderID="MainContent">
    <h3 id="page_title" runat="server"></h3>
    <div id="page_author" runat="server"></div>
    <div id="page_publication_date" runat="server"></div>
    <div id="page_content" runat="server"></div>

    <%--DataSource which gets information on the page selected--%>
    <asp:SqlDataSource runat="server"
        id="page_selected"
        ConnectionString="<%$ ConnectionStrings:pages_sql_con %>">
    </asp:SqlDataSource>

    <br />
    
    <asp:Button id="update_page_btn" runat="server" Text="Update Page" OnClick="UpdatePage" ></asp:Button>
    <asp:Button id="delete_page_btn" runat="server" Text="Delete Page" OnClick="DeletePage" OnClientClick="return confirm('Are you sure you want to delete this page?')" ></asp:Button>
    <a href="ManagePages">Back to Manage Pages</a>

</asp:Content>
