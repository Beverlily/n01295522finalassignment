﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace BeverlyLi_FinalProject
{
    public partial class Page : System.Web.UI.Page
    {
        private string baseSelectQuery = "SELECT page_title AS 'Page Title', page_author AS 'Page Author', page_publication_date AS 'Page Publication Date', page_content AS 'Page Content' FROM pages";
        //Gets page_id from URL 
        private string page_id
        {
            get { return Request.QueryString["page_id"]; }
        }

        //Based off CRUD example
        protected void Page_Load(object sender, EventArgs e)
        {
            if (page_id == "" || page_id == null) page_title.InnerHtml = "No page found.";
            else
            {
                //Select command for the SQLDataSource to retrieve information from the database, in this case its where page_id = specific number
                page_selected.SelectCommand = baseSelectQuery + " WHERE page_id=" + page_id;

                //Will only have one row because for select statement did where page_id (primary key) = specific number
                DataView currentPageView = (DataView)page_selected.Select(DataSourceSelectArguments.Empty);

                //First/only row = information of the single page 
                DataRowView currentPageRow = currentPageView[0]; 

                //Displays the information in HTML
                page_title.InnerHtml = currentPageRow["Page Title"].ToString();
                page_author.InnerHtml = "Author: " + currentPageRow["Page Author"].ToString();
                page_publication_date.InnerHtml = "Published on: " + currentPageRow["Page Publication Date"].ToString();
                page_content.InnerHtml = currentPageRow["Page Content"].ToString();

            }
        }

        protected void UpdatePage(object sender, EventArgs e)
        {
            //Page.aspx?page_id=__
            string updateUrl = "UpdatePages.aspx?page_id=" + page_id;
            //redirects to Update Page interface 
            Response.Redirect(updateUrl);
        }

        protected void DeletePage(object sender, EventArgs e)
        {
            Response.Redirect("DeletePages.aspx?page_id="+page_id);
        }



    }
}