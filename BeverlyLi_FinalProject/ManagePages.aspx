﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ManagePages.aspx.cs" Inherits="BeverlyLi_FinalProject.Pages" %>
<asp:Content ID="view_pages" ContentPlaceHolderID="MainContent" runat="server">
        <h2>Manage Pages</h2>
        <a href="AddPages">Add Page</a>

        <br />
        <br />
        
        <%--Search... Referenced your Students example--%>
        <asp:TextBox runat="server" ID="pageSearch"></asp:TextBox>
        <asp:Button runat="server" Text="Search" OnClick="Search_Pages"/>

        <br />
        <br />

        <%--Data source used to get information on pages--%>
        <asp:SqlDataSource runat="server" id="pages"
            ConnectionString="<%$ ConnectionStrings:pages_sql_con %>">
        </asp:SqlDataSource>

        <%--Datagrid to displays list of pages--%> 
        <asp:DataGrid ID="page_list" runat="server"></asp:DataGrid>

</asp:Content>
