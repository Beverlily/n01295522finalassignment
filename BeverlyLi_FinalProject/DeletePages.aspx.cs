﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BeverlyLi_FinalProject
{
    public partial class WebForm2 : System.Web.UI.Page
    {
        private string page_id
        {
            get { return Request.QueryString["page_id"]; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (page_id == "" || page_id == null) message.InnerHtml = "No page found.";
            else
            {
                page_selected.DeleteCommand = "DELETE FROM pages WHERE page_id = '" + page_id + "'";
                page_selected.Delete();
                Response.Redirect("ManagePages");
            }
        }
    }
}