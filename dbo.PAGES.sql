﻿CREATE TABLE [dbo].[PAGES] (
    [page_id]               INT           IDENTITY (1, 1) NOT NULL,
    [page_title]            VARCHAR (50)  NOT NULL,
    [page_author]           VARCHAR (50)  NULL,
    [page_publication_date] DATETIME      DEFAULT (sysdatetime()) NOT NULL,
    [page_content]          VARCHAR (MAX) NULL,
    PRIMARY KEY CLUSTERED ([page_id] ASC)
);

